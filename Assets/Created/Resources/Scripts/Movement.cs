﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Conjunto de librerias de unity

public class Movement : MonoBehaviour //Clase principal del script.
{
	public float speed = 4f; /*Se declara el valor de la velocidad del personaje, en este caso es publica para ser modificable
	sin entrar directamente al script del movimiento*/
	Animator animu;
	Rigidbody rb2; // Se declara el motor de fisicas del personaje.
	Vector3 movement; // Se decalara el vector que guarda las posiciones del personaje.
    private float tiempo;
    spawner spawner;
    private int objetos = 0;
    public Text puntos, mensaje;

	void Start (){ //Es una ejecucion que se realiza en el inicio de la aplicacion.
        animu = GetComponent<Animator>();

        rb2 = GetComponent<Rigidbody>();

        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<spawner>();
        ActualizarPuntaje();
    }

	void Update () //Esto indica que es una funcion que se usara en tiempo de ejecucion de la aplicacion
	{
		movement = new Vector3 (Input.GetAxisRaw("Horizontal"),
								0f, Input.GetAxisRaw("Vertical"
                                ));/*Se declara un vector de dos
		dimensiones que en este caso detecta el movimiento del objeto en cuestion por los ejes X,Y*/
		if (movement != Vector3.zero) {
			animu.SetBool ("Walking", true);
			animu.SetFloat ("movX", movement.x);
			animu.SetFloat ("movY", movement.z);
		} else {
			animu.SetBool ("Walking", false);
		}
        Debug.Log(objetos);
	}

	void FixedUpdate (){
		rb2.MovePosition(rb2.position + movement * speed * Time.deltaTime);/*Esta funcion es la encargada de generar el movimiento
		                                                                   y de actualizar la posicion en el motor de fisicas*/
            
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        objetos++;
        ActualizarPuntaje();

        if (objetos == 5)
        {
            mensaje.gameObject.SetActive(true);
            StartCoroutine(QuitarDialogo());
        }
    }

    private IEnumerator QuitarDialogo()
    {
        yield return new WaitForSeconds(2f);
        mensaje.CrossFadeAlpha(0f, 1f, false);
        spawner.spawn();
        yield return new WaitForSeconds(2f);
        mensaje.gameObject.SetActive(false);
    }

    private void ActualizarPuntaje()
    {
        puntos.text = "Puntos: " + objetos;
    }

}
