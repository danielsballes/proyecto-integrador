﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentScript : MonoBehaviour {

    private NavMeshAgent agent;
    Transform target;

    // Use this for initialization
    void Start () {
        transform.parent = GameObject.Find("mapa").transform;
        target = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>(); //Se obtiene referencia al componente NavMeshAgent del agent que se va a mover
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        agent.SetDestination(target.position); //Esta linea es la que hace que el cubo del mundo 3d se mueva
    }
}
