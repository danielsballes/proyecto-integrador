﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class giroNave : MonoBehaviour {

    Vector3 velocidadRotacion = new Vector3(4, 20, 4);

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(velocidadRotacion * Time.deltaTime);
    }
}
