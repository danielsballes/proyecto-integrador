﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Follows : MonoBehaviour {

    public float speed;

    public Transform target;

    // Use this for initialization
    void Update() {
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime); 
	}
	
}
