﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    //Esta funcion podra spawnear enemigos y agentes con respecto a cierto tiempo
    public void spawn()
    {
        GameObject axeMan = Resources.Load<GameObject>("Prefabs/axeMan");
        GameObject Agent = Resources.Load<GameObject>("Prefabs/Agent");
        Instantiate(Agent, new Vector3(-1.824048f, 17.17251f, -9.4f), Quaternion.Euler(90f, 0f, 0f));
        Instantiate(axeMan, new Vector3(-1.824048f, 17.17251f, 0f), Quaternion.Euler(90f, 0f, 0f));
        Destroy(gameObject);
    }
}
