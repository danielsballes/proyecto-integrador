﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteScript : MonoBehaviour {

    Transform target; //El objetivo que va a seguir este objeto
    Transform player; //Se obtiene la posicion del jugador para calcular la direccion del objeto
    private Animator aserrin; //Es el animador de este objeto
    public float yOffSet; 
    private Vector3 heading; //Direccion del objeto

    // Use this for initialization
    void Start () {
        transform.parent = GameObject.Find("2dmap").transform;
        target = GameObject.FindGameObjectWithTag("Agent").transform;
        player = GameObject.Find("Player").transform;
        aserrin = GetComponent<Animator>(); //Se obtiene referencia al componente animador del objeto que tenga este script
    }
	
	// Update is called once per frame
	void Update () {
	}

    void LateUpdate()
    {
        //La linea de abajo es la que mueve el objeto
        transform.localPosition = new Vector3(target.localPosition.x, target.localPosition.y - yOffSet, 0);
        heading = player.position - transform.position; //Esto obtiene la direccion del objeto
        heading = heading.normalized; //Se normaliza el vector en terminos de (1,-1)

        aserrin.SetFloat("x", heading.x); //Estas lineas le mandan un parametro al animador para que sepa cual animacion
        aserrin.SetFloat("y", heading.z); //debe usar en cada frame del juego.

        Debug.Log(heading.normalized);
    }
}
