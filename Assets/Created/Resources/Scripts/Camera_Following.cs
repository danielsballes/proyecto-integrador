﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Following : MonoBehaviour {

    public GameObject player;
    public float suavizado = 5f;
    Vector3 desface; 

    private void Start() {
        Invoke("Desface", 0.5f);
    }

    void Desface()
    {
        desface = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void FixedUpdate () {
        Vector3 Posicion_Objetivo = player.transform.position + desface;
        transform.position = Vector3.Lerp(transform.position, Posicion_Objetivo, suavizado * Time.deltaTime);
	}
}
