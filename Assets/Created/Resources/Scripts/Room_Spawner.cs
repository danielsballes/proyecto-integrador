﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room_Spawner : MonoBehaviour {

    public Room_Template template;
     
    private int rand;

    void Start()
    {
        template = GameObject.FindGameObjectWithTag("Rooms").GetComponent<Room_Template>();
        Invoke("Spawn", 0.1f);
    }

    void Spawn()
    {
        rand = Random.Range(0, template.Rooms.Length);
        Instantiate(template.Rooms[rand], transform.position, template.Rooms[rand].transform.rotation);
        Destroy(gameObject);
    }

    /*void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("SpawnPoint"))
        {
            if((other.GetComponent<Room_Spawner>().spawned == false) && (spawned == false))
            {
                Destroy(gameObject);
            }
            spawned = true;
        }
    }*/
}
